import React from 'react';
import { StyleSheet, Text, View, Slider } from 'react-native';
import { StatusBar } from 'react-native';
import { Alert, AppRegistry, Platform} from 'react-native';
import DoubleClick from 'react-native-double-click';
import Circle from './Circle';


export default class App extends React.Component {
    constructor(props) {
	super(props);
	this.state = {
	    circles: {},
	    color: {
		red: 100,
		green: 20,
		blue: 30
	    }
	};
    }

    debug(text){
	console.log(text);
    }
    
    render() {
	return (
	    <View style={styles.container}>
	    <StatusBar
		backgroundColor="blue"
		barStyle="light-content"
		hidden={true}
	    />
	    <DoubleClick onClick={(event) => this._onPressButton(event)}
			 style={{flex: 1}}>
		{Object.keys(this.state.circles).map((item, index) => {
		     return this.state.circles[item];
		})}
	    </DoubleClick>
	    <View style={{backgroundColor: 'lightgrey', padding: 10}}>
		<Slider
		    step={1}
		    maximumValue={255}
		    onValueChange={(newValue) => {
			    let color = this.state.color;
			    color.red = newValue;
			    this.setState({color: color});}
		    }
		    value={this.state.color.red}
		    minimumTrackTintColor="red"
		/>
		<Slider
		    step={1}
		    maximumValue={255}
		    onValueChange={(newValue) => {
			    let color = this.state.color;
			    color.green = newValue;
			    this.setState({color: color});}
		    }
		    value={this.state.color.green}
		    minimumTrackTintColor="green"
		/>
		<Slider
		    step={1}
		    maximumValue={255}
		    onValueChange={(newValue) => {
			    let color = this.state.color;
			    color.blue = newValue;
			    this.setState({color: color});}
		    }
		    value={this.state.color.blue}
		    minimumTrackTintColor="blue"
		/>
	    </View>
	    </View>
	);
    }

    unmountCircle(id){
	delete this.state.circles[id];
	this.setState({circles: this.state.circles});
    }
    
    _onPressButton(event){
	// try to detect double tap
	let x = event.nativeEvent.locationX;
	let y = event.nativeEvent.locationY;
	let circleId = guid();
	let circle = <Circle x={x} y={y}
			     debug={(text) => {this.debug(text);}}
			     key={circleId}
			     onClick={() => {this.unmountCircle(circleId);}}
			     color={`rgb(${this.state.color.red}, ${this.state.color.green}, ${this.state.color.blue})`}
			     id={circleId} />;
	this.state.circles[circleId] = circle;
	this.setState({
	    circles: this.state.circles
	});
    }
}

const styles = StyleSheet.create({
    container: {
	flex: 1,
	display: 'flex',
    }
});

function guid() {
    function s4() {
	return Math.floor((1 + Math.random()) * 0x10000)
		   .toString(16)
		   .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}


