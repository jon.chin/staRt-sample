import React, {Component} from 'react';
import {View, PanResponder, Alert, Animated} from 'react-native';
import PropTypes from 'prop-types';

export default class Circle extends React.Component{
    constructor(props) {
	super(props);
	this.myPanResponder = {};

	this.prevTouchInfo = {
	    prevTouchX: 0,
	    prevTouchY: 0,
	    prevTouchTimeStamp: 0,
	};

	this.handlePanResponderGrant = this.handlePanResponderGrant.bind(this);
	this.state = {
	    pan: new Animated.ValueXY(),
	    scale: new Animated.Value(1)
	};
    }

    componentWillMount() {
	this.myPanResponder = PanResponder.create({
	    onMoveShouldSetResponderCapture: () => true,
	    onMoveShouldSetPanResponderCapture: () => true,
	    onStartShouldSetPanResponder: (evt, gestureState) => true,
	    onPanResponderGrant: this.handlePanResponderGrant,
	    onPanResponderMove: Animated.event([
		null, {dx: this.state.pan.x, dy: this.state.pan.y},
	    ]),
	    onPanResponderRelease: (e, {vx, vy}) => {
		// Flatten the offset to avoid erratic behavior
		this.state.pan.flattenOffset();
		Animated.spring(
		    this.state.scale,
		    { toValue: 1, friction: 3 }
		).start();
	    }
	});
    }

    distance(x0, y0, x1, y1) {
	return Math.sqrt(Math.pow(x1 - x0, 2) + Math.pow(y1 - y0, 2));
    }

    isDoubleTap(currentTouchTimeStamp, {x0, y0}) {
	const {prevTouchX, prevTouchY, prevTouchTimeStamp} = this.prevTouchInfo;
	const dt = currentTouchTimeStamp - prevTouchTimeStamp;
	const {delay, radius} = this.props;

	return dt < delay && this.distance(prevTouchX, prevTouchY, x0, y0) < radius;
    }

    handlePanResponderGrant(evt, gestureState) {
	const currentTouchTimeStamp = Date.now();

	if (this.isDoubleTap(currentTouchTimeStamp, gestureState)) {
	    this.props.onClick(evt, gestureState);
	}

	this.prevTouchInfo = {
	    prevTouchX: gestureState.x0,
	    prevTouchY: gestureState.y0,
	    prevTouchTimeStamp: currentTouchTimeStamp,
	};

	//////////////////////////////
	// Set the initial value to the current state
	this.state.pan.setOffset({x: this.state.pan.x._value, y: this.state.pan.y._value});
	this.state.pan.setValue({x: 0, y: 0});
	Animated.spring(
	    this.state.scale,
	    { toValue: 1.1, friction: 3 }
	).start();
	//////////////////////
	
	
    }

    render(){
	//////////////////
	// Destructure the value of pan from the state
	let { pan, scale } = this.state;
	// Calculate the x and y transform from the pan value
	let [translateX, translateY] = [pan.x, pan.y];

	let radius = 50;
	let style = {
	    position: 'absolute',
	    top: this.props.y - radius,
	    left: this.props.x - radius,
	    width: radius * 2,
	    height: radius * 2,
	    borderRadius: radius,
	    backgroundColor: this.props.color,
	    transform: [{translateX}, {translateY}]
	};	
	return (
	   <Animated.View style={style}
	   {...this.props}
	   {...this.myPanResponder.panHandlers}
	   />
	);
    }
}

Circle.defaultProps = {
    delay: 300,
    radius: 20,
    onClick: () => {},
};

Circle.propTypes = {
    delay: PropTypes.number,
    radius: PropTypes.number,
    onClick: PropTypes.func,
    unmount: PropTypes.func
};

module.exports = Circle;
